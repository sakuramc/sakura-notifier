package ch.verttigo.notifier;

import ch.verttigo.notifier.Listener.JoinQuitListener;
import ch.verttigo.notifier.Listener.cheatListener;
import ch.verttigo.notifier.SoftDepends.Vanish;
import ch.verttigo.notifier.commands.check_CMD;
import ch.verttigo.notifier.database.RedisConnector;
import ch.verttigo.notifier.database.RedisMethods;
import ch.verttigo.notifier.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;

public class Notifier extends JavaPlugin {


    public static HashMap<Player, Boolean> isUsingBAC = new HashMap<Player, Boolean>();
    public static Boolean onRedisDB = false;

    public static Notifier instance;

    public static Notifier getInstance() {
        return Notifier.instance;
    }

    @Override
    public void onEnable() {
        super.onEnable();
        //Instance
        Notifier.instance = this;

        //Commands
        this.getCommand("baccheck").setExecutor(new check_CMD());

        //Bungeecord Messaging
        this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        this.getServer().getMessenger().registerIncomingPluginChannel(this, "BungeeCord", new Utils());

        //Config
        this.getConfig().options().copyDefaults(true);
        this.saveConfig();

        //Register Listener
        this.getServer().getPluginManager().registerEvents(new JoinQuitListener(), this);

        //Check API
        if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null && Bukkit.getPluginManager().getPlugin("PremiumVanish") != null) {
            Bukkit.getPluginManager().registerEvents(new Vanish(), this);
            new Vanish().register();
        } else {
            System.out.println("[SNotifier] La fonction PlaceHolder n'est pas active du a un manque de plugin.");
        }

        //Checking AntiCheat
        if (Bukkit.getPluginManager().getPlugin("Spartan") != null) {
            Bukkit.getPluginManager().registerEvents(new cheatListener(), this);
        } else {
            System.out.println("[SNotifier] La fonction Anti-cheat n'est pas active du a un manque de plugin.");
        }

        //Redis Connexion
        new RedisMethods(this);

        //OnReload get back BAC
        for (Player p : Bukkit.getOnlinePlayers()) {
            Utils.checkPlayer(p);
        }
    }

    @Override
    public void onDisable() {
        super.onDisable();

    }
}