package ch.verttigo.notifier.Listener;

import ch.verttigo.notifier.Notifier;
import ch.verttigo.notifier.database.RedisMethods;
import ch.verttigo.notifier.utils.Utils;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class JoinQuitListener implements Listener {


    @EventHandler
    public void onLogin(PlayerJoinEvent event) {
        Utils.checkPlayer(event.getPlayer());
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent event) {
        if (Notifier.onRedisDB) {
            //RedisMethods.removePlayerBACRedis(event.getPlayer());
        } else {
            Notifier.isUsingBAC.remove(event.getPlayer());
        }
    }
}
