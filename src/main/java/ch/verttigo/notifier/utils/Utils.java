package ch.verttigo.notifier.utils;

import ch.verttigo.notifier.Notifier;
import ch.verttigo.notifier.database.RedisMethods;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.io.*;
import java.util.HashMap;

public class Utils implements PluginMessageListener {

    public Utils() {

    }

    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] bytes) {
        if (channel.equals("BungeeCord")) {
            DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(bytes));

            try {
                if (dataInputStream.readUTF().equals("heartbeat")) {
                    String s = dataInputStream.readUTF();
                    if (s.equalsIgnoreCase("true")) {
                        if (Notifier.onRedisDB) {
                            if (!RedisMethods.getAllBACUsersRedis().containsKey(player.getUniqueId()))
                                RedisMethods.setPlayerBACRedis(player, true);
                        } else {
                            if (!Notifier.isUsingBAC.containsKey(player)) Notifier.isUsingBAC.put(player, true);
                        }
                    } else if (s.equalsIgnoreCase("false")) {
                        if (Notifier.onRedisDB) {
                            if (!RedisMethods.getAllBACUsersRedis().containsKey(player.getUniqueId()))
                                RedisMethods.setPlayerBACRedis(player, false);
                        } else {
                            if (!Notifier.isUsingBAC.containsKey(player)) Notifier.isUsingBAC.put(player, false);
                        }
                    }
                }
            } catch (IOException ignored) {

            }
        }
    }


    public static int BACUsingIt() {
        int count = 0;
        if (Notifier.onRedisDB) {
            for (Boolean values : RedisMethods.getAllBACUsersRedis().values()) {
                if (values) {
                    count++;
                }
            }
        } else {
            for (Boolean target : Notifier.isUsingBAC.values()) {
                if (target) {
                    count++;
                }
            }
        }
        return count;
    }

    public HashMap<Player, Boolean> getIsUsingBAC() {
        return Notifier.isUsingBAC;
    }

    public static boolean isBACPlayer(Player p) {
        if (Notifier.onRedisDB) {
            return RedisMethods.getPlayerBACRedis(p);
        } else {
            return Notifier.isUsingBAC.get(p);
        }
    }

    public static boolean playerExist(Player p) {
        boolean exist;
        if (Notifier.onRedisDB) {
            try {
                String s = p.getUniqueId().toString();
                exist = true;
            } catch (Exception e) {
                exist = false;
            }
        } else {
            if (Notifier.isUsingBAC.containsKey(p)) {
                exist = true;
            } else {
                exist = false;
            }
        }
        return exist;
    }

    public static void checkPlayer(final Player p) {
        Bukkit.getScheduler().runTaskLater(Notifier.getInstance(), () -> {

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);

            try {
                dataOutputStream.writeUTF("heartbeat");
            } catch (IOException ignored) {
                return;
            }
            p.sendPluginMessage(Notifier.getInstance(), "BungeeCord", byteArrayOutputStream.toByteArray());
        }, 20L);
    }


}
