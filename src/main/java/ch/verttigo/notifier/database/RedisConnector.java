package ch.verttigo.notifier.database;

import ch.verttigo.notifier.Notifier;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class RedisConnector {

    private final String cacheIp;
    private final String password;
    protected JedisPool cachePool;
    protected Jedis jedis;

    public RedisConnector(String cacheIp, String password) {
        this.cacheIp = cacheIp;
        this.password = password;
        initiateConnections();
    }


    public Jedis getCacheResource() {
        return cachePool.getResource();
    }

    public void killConnections() {
        cachePool.destroy();
    }

    public void initiateConnections() {
        System.out.println("[Snotifier] Starting Redis caching connection.");
        // Préparation de la connexion
        this.cachePool = new JedisPool(new JedisPoolConfig(), cacheIp);
        try (Jedis jedis = cachePool.getResource()) {
            jedis.auth(password);
            this.jedis = jedis;
            Notifier.onRedisDB = true;
            System.out.println("[Snotifier] Redis connection initialized.");
        } catch (Exception e) {
            System.out.println("[Snotifier] Database is not online or not set in the config file, can't connect lol");
            Notifier.onRedisDB = false;
        }
    }


    public void disable() {
        System.out.println("[Snotifier] Removing cache pools...");
        killConnections();
    }

}

