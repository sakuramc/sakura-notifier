package ch.verttigo.notifier.database;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Response;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class RedisMethods {

    public static RedisConnector RedisConnector;
    static Pipeline pipeline;


    public RedisMethods(Plugin p) {
        RedisConnector = new RedisConnector(p.getConfig().getString("Redis.IP"), p.getConfig().getString("Redis.Password"));
        pipeline = RedisConnector.jedis.pipelined();
    }

    public static void destroy() {
        RedisConnector.disable();
    }

    public static void setPlayerBACRedis(Player p, Boolean isBac) {
        pipeline.hset("Snotifier", p.getUniqueId().toString(), isBac.toString());
        pipeline.sync();
    }

    public static void removePlayerBACRedis(Player p) {
        RedisConnector.getCacheResource().hdel("Snotififer", p.getUniqueId().toString());
        pipeline.sync();
    }

    public static boolean getPlayerBACRedis(Player p) {
        Response<String> isUsingIt = pipeline.hget("Snotifier", p.getUniqueId().toString());
        pipeline.sync();
        return Boolean.parseBoolean(isUsingIt.toString());
    }

    public static HashMap<UUID, Boolean> getAllBACUsersRedis() {
        Response<Map<String, String>> mapFromRedis = pipeline.hgetAll("Snotifier");
        pipeline.sync();

        HashMap<UUID, Boolean> mapWithType = new HashMap<>();
        for (String uuid : mapFromRedis.get().keySet()) {
            mapWithType.put(UUID.fromString(uuid), Boolean.parseBoolean(mapFromRedis.get().get(uuid)));
        }
        return mapWithType;
    }

}
