package ch.verttigo.notifier.SoftDepends;

import ch.verttigo.notifier.Notifier;
import de.myzelyam.api.vanish.VanishTargetChangeEvent;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class Vanish extends PlaceholderExpansion implements Listener {

    static Player target = null;
    static Boolean isUsingIt = false;


    @EventHandler
    public static void onTargetChangeVanish(VanishTargetChangeEvent e) {
        if (e.getNewTarget() != null) {
            target = e.getNewTarget();
            if (Notifier.isUsingBAC.get(target) != null) {
                isUsingIt = Notifier.isUsingBAC.get(target);
            }
        }
    }

    //PLACEHOLDER

    @Override
    public boolean canRegister() {
        return true;
    }

    @Override
    public String getAuthor() {
        return "Verttigo";
    }

    @Override
    public boolean persist() {
        return true;
    }

    @Override
    public String getIdentifier() {
        return "Notif";
    }

    @Override
    public String getVersion() {
        return "1.0.0";
    }

    @Override
    public String onPlaceholderRequest(Player p, String identifier) {

        //Return the placeholder %Notif_BAC%
        if (identifier.equals("BAC")) {
            if (target == null) {
                return "None";
            } else {
                return isUsingIt.toString();
            }

        }
        return null;
    }
}
