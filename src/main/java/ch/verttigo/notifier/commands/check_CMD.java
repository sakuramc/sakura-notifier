package ch.verttigo.notifier.commands;

import ch.verttigo.notifier.Notifier;
import ch.verttigo.notifier.database.RedisMethods;
import ch.verttigo.notifier.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

public class check_CMD implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String msg, String[] args) {
        final Player p = (Player) sender;
        if (cmd.getName().equalsIgnoreCase("baccheck") && p.hasPermission("snotifier.admin")) {
            if (p instanceof Player) {
                if (args.length == 0) {
                    if (Utils.isBACPlayer(p)) {
                        p.sendMessage("§eYou are using BAC");
                    } else {
                        p.sendMessage("§6You are not using BAC");
                    }
                    return false;
                }
            }
            if (args.length >= 1) {
                if (args[0] != null && !args[0].equalsIgnoreCase("list")) {
                    Player t1 = Bukkit.getPlayer(args[0]);
                    if (!Utils.playerExist(t1)) {
                        p.sendMessage("Unknown Player");
                    } else {
                        if (Notifier.onRedisDB) {
                            p.sendMessage("» §eIs §6" + t1.getDisplayName() + " §eusing BAC? : §6" + RedisMethods.getPlayerBACRedis(t1));
                        } else {
                            for (Player target : Notifier.isUsingBAC.keySet()) {
                                if (target == t1) {
                                    p.sendMessage("» §eIs §6" + target.getDisplayName() + " §eusing BAC? : §6" + Notifier.isUsingBAC.get(target));
                                }
                            }
                        }
                    }
                }
                if (args[0].equalsIgnoreCase("list")) {
                    p.sendMessage("§7§m-------------------------------");
                    if (Notifier.onRedisDB) {
                        p.sendMessage("§6Number of Badlion Client on the network: §e" + Utils.BACUsingIt());
                        p.sendMessage("§6Player who's using BAC:");
                        for (UUID uuid : RedisMethods.getAllBACUsersRedis().keySet()) {
                            if(RedisMethods.getAllBACUsersRedis().get(uuid)) {
                                p.sendMessage("§e" + Bukkit.getPlayer(uuid).getDisplayName() + "§6,");
                            } else {
                                p.sendMessage("§eNo one is using BAC Client");
                            }
                        }
                    } else {
                        p.sendMessage("§6Number of Badlion Client in this server: §e" + Utils.BACUsingIt());
                        for (Player target : Notifier.isUsingBAC.keySet()) {
                            p.sendMessage("» §eIs §6" + target.getDisplayName() + " §eusing BAC? : §6" + Notifier.isUsingBAC.get(target));
                        }
                    }
                    p.sendMessage("§7§m--------------------------------");
                }
            }
        }
        return false;

    }


}
